import time
from .invoice import Invoice


class Payment:
    authorization_number = None
    amount = None
    invoice = None
    order = None
    payment_method = None
    _is_paid = False

    def __init__(self, attributes={}):
        self.authorization_number = attributes.get('attributes', None)
        self.amount = attributes.get('amount', None)
        self.invoice = attributes.get('invoice', None)
        self.order = attributes.get('order', None)
        self.payment_method = attributes.get('payment_method', None)

    def pay(self):
        self.amount = self.order.total_amount
        self.authorization_number = int(time.time())
        self._is_paid = True
        print('>>> Payment Authorization')

        """
        Create Invoice
        """
        self.invoice = Invoice(self.order)
        self.invoice.view()

        """
        Product Process
        """
        stdout_list = []
        print('>>> Process')
        for item in self.order.items:
            stdout = item.product.process(customer=self.order.customer)
            stdout_list.append(stdout)

        return stdout_list

    @property
    def is_paid(self):
        return self._is_paid


class CreditCard:
    @staticmethod
    def fetch_by_hashed(code):
        return CreditCard()
