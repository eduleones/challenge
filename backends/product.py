from .shipping import Shipping
from .voucher import Voucher
from .email import Email


class Product(object):
    name = None
    description = None

    def __init__(self, name, description):
        self.name = name
        self.description = description

    def __str__(self):
        return self.name

    def _process(self, customer):
        pass


class Physical(Product):

    @property
    def type(self):
        return 'physical'

    def process(self, customer):
        return self._process(customer)

    def _process(self, customer):
        message = 'Send Package to {} in {}'.format(customer.name, customer.address)
        Shipping(customer).print_label(message)
        return '_process_physical_true'


class Book(Product):

    @property
    def type(self):
        return 'book'

    def process(self, customer):
        return self._process(customer)

    def _process(self, customer):
        message = 'Send Package to {} in {}'.format(customer.name, customer.address)
        message += '- Free tax - Constitution Art. 150, VI, d'
        Shipping(customer).print_label(message)
        return '_process_book_true'


class Digital(Product):

    @property
    def type(self):
        return 'digital'

    def process(self, customer):
        return self._process(customer)

    def _process(self, customer):
        print('>>> Added Voucher: 10.00')
        voucher = Voucher(customer)
        voucher.add_voucher(10)
        print('Total Voucher: {}'.format(voucher.total_amount))

        print('>>> Notification - Send Email:')
        message = '{}, you purchased {} and won a voucher of R$ 10'.format(customer.name, self.name)
        notification = Email(customer.email)
        notification.send_mail(message)
        return '_process_digital_true'


class Membership(Product):

    @property
    def type(self):
        return 'membership'

    def process(self, customer):
        return self._process(customer)

    def _process(self, customer):
        print('>>> Activate Membership')
        customer.has_membership = True
        print('Membership Status: {}'.format(customer.has_membership))

        print('>>> Notification - Send Email:')
        message = '{}, you purchased um new service: {}'.format(customer.name, self.name)
        notification = Email(customer.email)
        notification.send_mail(message)
        return '_process_membership_true'
