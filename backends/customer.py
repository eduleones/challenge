class Customer:
    """
        Customer Class
    """
    name = None
    email = None
    address = None
    _has_membership = False

    def __init__(self, name, email, address):
        self.name = name
        self.email = email
        self.address = address

    @property
    def has_membership(self):
        return self._has_membership

    @has_membership.setter
    def has_membership(self, value):
        self._has_membership = value
