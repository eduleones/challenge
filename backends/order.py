class Order:
    customer = None
    items = None
    payment = None

    def __init__(self, customer):
        self.customer = customer
        self.items = []

    def add_product(self, product, amount):
        self.items.append(OrderItem(product=product, amount=amount))

    @property
    def total_amount(self):
        total = 0
        for item in self.items:
            total += item.amount

        return total


class OrderItem:
    product = None
    amount = None

    def __init__(self, product, amount):
        self.product = product
        self.amount = amount

    def __str__(self):
        return self.product.name
