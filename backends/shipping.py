class Shipping:

    customer = None

    def __init__(self, customer):
        self.customer = customer

    def print_label(self, message):
        print('>>> Create Shipping Label:')
        print('{}'.format(message))
        return message
