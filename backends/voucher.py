class Voucher:
    customer = None
    amount = 0

    def __init__(self, customer):
        self.customer = customer

    def add_voucher(self, amount):
        self.amount += amount

    @property
    def total_amount(self):
        return self.amount
