class Email:
    email = None

    def __init__(self, email):
        self.email = email

    def send_mail(self, message):
        print('>>> Send Mail')
        print('to: {}'.format(self.email))
        print('msg: {}'.format(message))
        return True
