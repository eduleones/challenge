class Invoice:
    order = None
    status = None

    def __init__(self, order):
        self.order = order
        self.status = 'created'

    def view(self):
        print('>>> Invoice created')
