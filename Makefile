flake8:
	@flake8 backends/ --show-source --ignore=E50
	@flake8 tests/ --show-source --ignore=E50

test:
	@py.test -x
