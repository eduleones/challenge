"""
Script from tests
"""

from backends.customer import Customer
from backends.order import Order
from backends.product import Physical, Book, Digital, Membership
from backends.payment import Payment, CreditCard

customer = Customer(name='José Fonseca', email='joseff@gmail.com', address='Rua Cometa, 122')
order = Order(customer)

product_1 = Physical(name='Capa Samsung A8', description='Capa de Celular para Samsumg A8')
product_2 = Book(name='Casa Monstro', description='Livro baseado na historia do filme')
product_3 = Digital(name='Rappa Mundi', description='O Rappa')
product_4 = Membership(name='Assinatura +', description='Serviço Premium')

order.add_product(product_1, 10)
order.add_product(product_2, 34.9)
order.add_product(product_3, 2.49)
order.add_product(product_4, 29.90)

attributes = dict(
    order=order,
    payment_method=CreditCard.fetch_by_hashed('43567890-987654367')
)
payment = Payment(attributes=attributes)
payment.pay()
