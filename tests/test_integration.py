from .factories import (create_new_customer,
                        create_product_physical,
                        create_product_book,
                        create_product_membership,
                        create_product_digital)

from backends.order import Order
from backends.payment import Payment, CreditCard


class TestPayment(object):

    def setup(self):
        self.customer = create_new_customer()

        self.product_physical = create_product_physical()
        self.product_book = create_product_book()
        self.product_membership = create_product_membership()
        self.product_digital = create_product_digital()

        self.order = Order(self.customer)

        attributes = dict(
            order=self.order,
            payment_method=CreditCard.fetch_by_hashed('43567890-987654367')
        )
        self.payment = Payment(attributes=attributes)

    def test_buy_all_products(self):

        self.order.add_product(self.product_physical, 100)
        assert self.order.total_amount == 100

        self.order.add_product(self.product_book, 50)
        assert self.order.total_amount == 150

        self.order.add_product(self.product_digital, 22)
        assert self.order.total_amount == 172

        self.order.add_product(self.product_membership, 39.90)
        assert self.order.total_amount == 211.90

        stdout = self.payment.pay()

        assert stdout[0] == '_process_physical_true'
        assert stdout[1] == '_process_book_true'
        assert stdout[2] == '_process_digital_true'
        assert stdout[3] == '_process_membership_true'
