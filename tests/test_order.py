from .factories import create_new_customer, create_product_physical

from backends.order import Order


class TestOrder(object):

    def setup(self):
        self.customer = create_new_customer()
        self.order = Order(self.customer)

        self.product_physical = create_product_physical()

    def test_create_order(self):

        assert self.order.customer.name == 'João da Silva'

    def test_add_product(self):

        assert len(self.order.items) == 0

        self.order.add_product(self.product_physical, 399)

        assert len(self.order.items) == 1
        assert self.order.total_amount == 399
