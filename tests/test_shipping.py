from .factories import create_new_customer

from backends.shipping import Shipping


class TestShipping(object):

    def setup(self):
        self.customer = create_new_customer()
        self.shipping = Shipping(self.customer)

    def test_print_label(self):

        message = 'Print Label Test'

        label = self.shipping.print_label(message)

        assert label == message
