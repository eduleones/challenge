from .factories import create_new_customer, create_product_physical

from backends.order import Order
from backends.invoice import Invoice


class TestInvoice(object):

    def setup(self):
        self.customer = create_new_customer()
        self.product = create_product_physical()
        self.order = Order(self.customer)
        self.order.add_product(self.product, 100)

    def test_invoice(self):

        invoice = Invoice(self.order)

        assert invoice.status == 'created'
