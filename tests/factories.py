from backends.customer import Customer
from backends.product import Physical, Book, Digital, Membership


def create_new_customer():
    customer = Customer(
        name='João da Silva',
        email='joao.silva@yahoo.com.br',
        address='Rua Amazonas, 121'
    )
    return customer


def create_product_physical():
    product = Physical(
        name='Suporte de Livros',
        description='Suporte Parede de Madeira para Livros',
    )
    return product


def create_product_book():
    product = Book(
        name='Harry Potter I',
        description='Harry Potter e a Pedra Filosofal'
    )
    return product


def create_product_digital():
    product = Digital(
        name='Rappa Mundi',
        description='Música Rappa Mundi - O Rappa'
    )
    return product


def create_product_membership():
    product = Membership(
        name='Plano +',
        description='Plano Premium'
    )
    return product
