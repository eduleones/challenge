from backends.email import Email


class TestEmail(object):

    def setup(self):
        self.email = Email(email='email@mail.com')

    def test_send_mail(self):
        message = 'Test Mail'

        send = self.email.send_mail(message)

        assert send
