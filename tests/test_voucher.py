from .factories import create_new_customer

from backends.voucher import Voucher


class TestVoucher(object):

    def setup(self):
        self.customer = create_new_customer()
        self.voucher = Voucher(self.customer)

    def test_add_voucher(self):

        assert self.voucher.total_amount == 0

        self.voucher.add_voucher(10)

        assert self.voucher.total_amount == 10
