from .factories import create_new_customer, create_product_physical

from backends.order import Order
from backends.payment import Payment, CreditCard


class TestPayment(object):

    def setup(self):
        self.customer = create_new_customer()
        self.product = create_product_physical()
        self.order = Order(self.customer)
        self.order.add_product(self.product, 100)

        attributes = dict(
            order=self.order,
            payment_method=CreditCard.fetch_by_hashed('43567890-987654367')
        )
        self.payment = Payment(attributes=attributes)

    def test_not_pay(self):

        assert not self.payment.is_paid

    def test_pay(self):

        self.payment.pay()

        assert self.payment.is_paid
