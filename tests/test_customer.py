from .factories import create_new_customer
from backends.customer import Customer


class TestCustomer(object):

    def setup(self):
        self.customer = create_new_customer()

    def test_create_customer(self):

        assert isinstance(self.customer, Customer)
        assert self.customer.name == 'João da Silva'

    def test_has_membership(self):

        assert not self.customer.has_membership

        self.customer.has_membership = True

        assert self.customer.has_membership
