from .factories import (create_product_physical,
                        create_product_book,
                        create_product_digital,
                        create_product_membership)


class TestProduct(object):

    def test_create_physical(self):

        physical = create_product_physical()
        assert physical.name == 'Suporte de Livros'

    def test_create_book(self):

        book = create_product_book()
        assert book.name == 'Harry Potter I'

    def test_create_digital(self):

        digital = create_product_digital()
        assert digital.name == 'Rappa Mundi'

    def test_create_membership(self):

        membership = create_product_membership()
        assert membership.name == 'Plano +'
